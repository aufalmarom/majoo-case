## Available Scripts

In the project directory, you can run:

## Git Versioning Explanation

We need team to build git flow. So, temporary I used this repository just for branch master. But, I can working together with team and any branches or git versioning.

### `npm run start` or `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build` or `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!
