import React, { Component } from 'react';
import ToDo from '../components/ToDo/ToDo';
import './List.css';
import { connect } from 'react-redux';
import ActionType from '../constants/actionType';
import FormModal from '../components/FormModal/FormModal';

class List extends Component {
  render() {
    return (
      <>
        <FormModal />
        <div className="row ml-1">
          <div className="col-md-6">
            <div className="row">
              <div className="col">
                <h1 className="header">To Do </h1> 
              </div>
              <div className="col btn-add">
                <button className="btn btn-primary" onClick={this.props.createFormModal}>Create</button>
              </div>
            </div>
              <ToDo data="toDo" />
          </div>
          <div className="col-md-6">
            <h1 className="header">To Do Done</h1> 
              <ToDo data="toDoDone" />
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isModal : state.isModal,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createFormModal: ()=> dispatch({type: ActionType.CREATE_FORM_MODAL}),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(List);
