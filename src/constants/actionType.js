const ActionType = {
  CREATE_FORM_MODAL: 'CREATE_FORM_MODAL',
  DESTROY_FORM_MODAL: 'DESTROY_FORM_MODAL',
  CREATE_TODO: 'CREATE_TODO',
  GET_TODO_LIST: 'GET_TODO_LIST',
  UPDATE_TODO: 'UPDATE_TODO',
  DELETE_TODO: 'DELETE_TODO',
};

export default ActionType;
