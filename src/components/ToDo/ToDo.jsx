import React, { Component } from 'react';
import { connect } from 'react-redux';
import ActionType from '../../constants/actionType';
import Button from '../Button/Button';

class ToDo extends Component {
  render() {
    return ( 
      this.props.data === 'toDo' ? this.props.toDo.map(el => {
        return (
          <div key={el.id} className="card mb-2">
              <div className="card-header"> 
              <h3 className="card-title">{el.title}</h3>
                <div className="card-body">
                <p>{el.description}</p> 
                <p >{el.createdAt}</p> 
                </div>
                <Button data={el} />
              </div>
          </div>
        )
      }) : this.props.toDoDone.map(el => {
        return (
          <div key={el.id} className="card mb-2">
              <div className="card-header"> 
              <h3 className="card-title">{el.title}</h3>
                <div className="card-body">
                <p>{el.description}</p> 
                <p >{el.createdAt}</p> 
                </div>
                <Button data={el} />
              </div>
          </div>
        )
      })
    )
  }
}

const mapStateToProps = (state) => {
  return {
    toDo : state.toDo.filter(el => el.status === 0),
    toDoDone : state.toDo.filter(el => el.status === 1),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createFormModal: ()=> dispatch({type: ActionType.CREATE_FORM_MODAL}),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDo);