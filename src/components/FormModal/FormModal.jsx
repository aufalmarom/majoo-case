import React, { Component } from 'react';
import './FormModal.css';
import { connect } from 'react-redux';
import ActionType from '../../constants/actionType';

class FormModal extends Component {
  state = {
    title: "",
    description: "",
  }

  handleChangeTitle = (e) => {
    e.preventDefault()
    this.setState({
      title: e.target.value,
    })
  }

  handleChangeDesc = (e) => {
    e.preventDefault()
    this.setState({
      description: e.target.value,
    })
  }

  buttonSubmit = (e) => {
    e.preventDefault()
    this.props.destroyFormModal()
    this.props.handleSubmit(
      { 
        id: new Date().getTime(), 
        title: this.state.title, 
        description : this.state.description, 
        status: 0,
        createdAt: new Date().getFullYear()+'-'+new Date().getMonth()+'-'+new Date().getDay()+' '+new Date().getHours()+':'+new Date().getMinutes()
      }
    )
  }
  

  render() {
    return (
      <>
      {
        <div className={this.props.isModal ? "modal-show" : "modal-hidden"}>
          <div className="modal-content">
          <span className="close" onClick={this.props.destroyFormModal}>&times;</span>
          <h5 className="mb-2">Form Create To Do</h5>
              <div className="form-group">
                  <label>Title</label>
                  <input type="text" name="title" value={this.state.title} onChange={this.handleChangeTitle} className="form-control" placeholder="Enter Title" />
              </div>
              <div className="form-group">
                  <label>Description</label>
                  <input type="text" name="description" value={this.state.description} onChange={this.handleChangeDesc} className="form-control" placeholder="Enter Description" />
              </div>
              <button type="submit" onClick={this.buttonSubmit} className="btn btn-primary">Submit</button>
          </div>
        </div>
      }
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isModal: state.isModal
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    destroyFormModal: ()=> dispatch({type: ActionType.DESTROY_FORM_MODAL}),
    updateData: (id)=> dispatch({type: ActionType.UPDATE_TODO, id}),
    handleSubmit: (payload)=> dispatch({type: ActionType.CREATE_TODO, payload}),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormModal);
