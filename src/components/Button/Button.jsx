import React, { Component } from 'react';
import { connect } from 'react-redux';
import ActionType from '../../constants/actionType';

class Button extends Component {
  render() {
    console.log(this.props)
    return ( 
        <div className="card-footer">
            <button className="btn btn-warning" onClick={() => this.props.createFormModal(this.props.data)}>Update</button>
            {
              this.props.data.status === 0 ? <button className="btn btn-danger ml-1" onClick={() => this.props.deleteData(this.props.data.id)}>Delete</button> : ""
            }
        </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createFormModal: (data) => dispatch({type: ActionType.CREATE_FORM_MODAL, data}),
    deleteData: (id)=> dispatch({type: ActionType.DELETE_TODO, id}),
  }
}

export default connect(null, mapDispatchToProps)(Button);