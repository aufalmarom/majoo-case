import ActionType from '../constants/actionType';

const initState = {
  toDo: [],
  isModal: false,
};

const rootReducer = (state = initState, action) => {
  switch (action.type) {
    case ActionType.CREATE_FORM_MODAL:
      return {
        ...state,
        isModal: true,
      };
    case ActionType.DESTROY_FORM_MODAL:
      return {
        ...state,
        isModal: false,
      };
    case ActionType.CREATE_TODO:
      return {
        ...state,
        toDo: [...state.toDo, action.payload],
      };
    case ActionType.GET_TODO_LIST:
      return {
        ...state,
        toDo: action.toDo,
      };
    case ActionType.UPDATE_TODO:
      return {
        ...state,
      };
    case ActionType.DELETE_TODO:
      return {
        ...state,
        toDo: state.toDo.filter((el) => el.id !== action.id),
      };
    default:
      break;
  }
  return state;
};

export default rootReducer;
