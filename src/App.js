import { Component } from 'react';
import List from '../src/containers/List';
import axios from 'axios';
import { connect } from 'react-redux';
import ActionType from './constants/actionType';

class App extends Component {
  getAPIToDo = () => {
    axios
      .get('https://virtserver.swaggerhub.com/hanabyan/todo/1.0.0/to-do-list')
      .then(
        (result) => {
          this.props.getAPIToDoList(result.data);
        },
        (err) => {
          console.log(err);
        }
      );
  };

  componentDidMount() {
    this.getAPIToDo();
  }

  render() {
    return <List />;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAPIToDoList: (toDo) =>
      dispatch({ type: ActionType.GET_TODO_LIST, toDo }),
  };
};

export default connect(null, mapDispatchToProps)(App);
